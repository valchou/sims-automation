// ======================================================================================
// UC Davis CAS login  
// 
// Author: Val Chou
// 
// User will send username and password via CLI 
// Script was created via ResurrectIO casperJS recorder
// 
// Usage: casperjs test testCase.js --UID=your_username --PWD=your_password
// 
// Revision: 0.01 (07/05/2016)
// 
// ======================================================================================

casper.sendLoginInfo = function(){
  var yourUsername = casper.cli.get('UID');
  var yourPassword = casper.cli.get('PWD');
 

   casper.wait(500, function(){
    
    this.fill('form#fm1', {
        'username' : yourUsername,
        'password' : yourPassword
    }, true);
    // this.capture("./images/loginPage.png");
  });
};